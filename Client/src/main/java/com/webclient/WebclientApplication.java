package com.webclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.awt.*;

@SpringBootApplication
public class WebclientApplication {

    public static void main(String[] args) throws Exception {
//        SpringApplication.run(WebclientApplication.class, args);
//    }
        var ctx = new SpringApplicationBuilder(com.webclient.WebclientApplication.class)
                .headless(false).run(args);

        EventQueue.invokeLater(() -> {
            var ex = ctx.getBean(com.webclient.userinput.UserInterface.class);
        });

    }
}
