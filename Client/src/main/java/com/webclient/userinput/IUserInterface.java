package com.webclient.userinput;

import com.webclient.model.Message;
import com.webclient.model.User;

import java.util.List;
import java.util.Queue;

public interface IUserInterface {
    void getUserCredentials(String userName, String address);

    void getUserMessages(String userMessage);

    void displayConnectedUserList(Queue<List<User>> userListQueue);
    void displayNewMessages(Queue<List<Message>> messageListQueue);

    void displayMessageHistory();

}
