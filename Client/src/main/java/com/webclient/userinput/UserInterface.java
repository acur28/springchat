package com.webclient.userinput;

import com.webclient.client.ApiClient;
import com.webclient.model.Message;
import com.webclient.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

@Component
public class UserInterface implements IUserInterface, Runnable {

    ApiClient myApiClient;
    private static final JFrame connectionJFrame = new JFrame();
    private static final JFrame chatJFrame = new JFrame();
    private static final JFrame connectionErrorJFrame = new JFrame();
    private static JTextArea chatMessageFeed;
    private static JTextArea connectedUserList;
    private Queue<List<User>> connectedUserListQueue;
    private Queue<List<Message>> sessionMessagesQueue;
    private List<String> localUserList;
    private List<UUID> localMsgList;

    @Autowired
    public UserInterface(ApiClient myApiClient) {
        initConnectionUI();
        initMainUI();
        localUserList = new ArrayList<>();
        localMsgList = new ArrayList<>();
        this.myApiClient = myApiClient;
    }

    private void initConnectionUI() {
        connectionJFrame.setTitle("SpringChat connection");
        connectionJFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // creating new elements
        JLabel l1, l2, l3;
        l1 = new JLabel("Username");
        l2 = new JLabel("adress");

        final JTextField usernametf = new JTextField();
        final JTextField passwordtf = new JTextField();

        JButton quitButton = new JButton("Quit");
        JButton connectButton = new JButton("Connect");
//        quitButton.setPreferredSize(buttondimension);
//        connectButton.setPreferredSize(buttondimension);

        // mapping callbacks on buttons
        connectButton.addActionListener((ActionEvent event) -> {
            getUserCredentials(usernametf.getText(), passwordtf.getText());
        });

        quitButton.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });

        // adding elements to the main pane
        JPanel mainConnectionPane = new JPanel();
        mainConnectionPane.add(l1);
        mainConnectionPane.add(usernametf);
        mainConnectionPane.add(Box.createRigidArea(new Dimension(0, 20)));
        mainConnectionPane.add(l2);
        mainConnectionPane.add(passwordtf);
        mainConnectionPane.add(Box.createRigidArea(new Dimension(0, 20)));
        mainConnectionPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainConnectionPane.setLayout(new BoxLayout(mainConnectionPane, BoxLayout.Y_AXIS));

        // adding elements to the button pane
        JPanel buttonPane = new JPanel();
        buttonPane.add(connectButton);
        buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonPane.add(quitButton);
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));

        //Put everything together
        Container contentPane = connectionJFrame.getContentPane();
        contentPane.add(mainConnectionPane, BorderLayout.CENTER);
        contentPane.add(buttonPane, BorderLayout.PAGE_END);

        connectionJFrame.setSize(325, 225);
        connectionJFrame.setVisible(true);

    }

    private void initMainUI() {

        chatJFrame.setTitle("SpringChat Client");
        chatJFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // creating new elements
        JLabel l1, l2, emptylabel;
        l1 = new JLabel("Connected users");
        l2 = new JLabel("Your message");
        emptylabel = new JLabel("");

        chatMessageFeed = new JTextArea(30, 25);
        chatMessageFeed.setLineWrap(true);
        connectedUserList = new JTextArea(15, 10);
        JTextField userMessage = new JTextField(25);

        JButton quitButton = new JButton("Quit");
        JButton msgButton = new JButton("Send Message");

        // mapping callbacks on buttons
        msgButton.addActionListener((ActionEvent event) -> {
            getUserMessages(userMessage.getText());
            userMessage.setText("");
        });

        quitButton.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });

        // adding elements to the left pane
        JPanel leftPane = new JPanel();
        leftPane.add(Box.createRigidArea(new Dimension(0, 10)));
        leftPane.add(chatMessageFeed);
        leftPane.add(Box.createRigidArea(new Dimension(0, 10)));
        leftPane.add(l2);
        leftPane.add(userMessage);
        leftPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 0));
        leftPane.setLayout(new BoxLayout(leftPane, BoxLayout.PAGE_AXIS));

        // adding elements to the right pane
        JPanel rightPane = new JPanel();
        rightPane.add(l1);
        rightPane.add(connectedUserList);
        rightPane.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPane.add(msgButton);
        rightPane.add(Box.createRigidArea(new Dimension(0, 10)));
        rightPane.add(quitButton);
        rightPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        rightPane.setLayout(new BoxLayout(rightPane, BoxLayout.PAGE_AXIS));

        //Put everything together
        Container contentPane = chatJFrame.getContentPane();
        contentPane.add(leftPane, BorderLayout.WEST);
        contentPane.add(rightPane, BorderLayout.EAST);
        chatJFrame.setSize(500, 650);

        chatJFrame.setVisible(false);
    }

    @Override
    public void getUserCredentials(String userName, String address) {
        try {
            // if the server response to the connection attempt with a HTTP 200 code, then display the main window
            boolean serverResponse = myApiClient.connectToServer(userName, address);
            if (serverResponse) {
                connectionJFrame.setVisible(false);
                chatJFrame.setVisible(true);
                new Thread(this).start();
            } else {
                // error dialog windows in case of HTTP response code other than 200
                JOptionPane.showMessageDialog(connectionErrorJFrame,
                        "Error connecting to the server \nPlease check your connection and credentials and try again",
                        "Connection error",
                        JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            // error dialog windows in case of failure
            JOptionPane.showMessageDialog(connectionErrorJFrame,
                    "Error while trying to connect to the server: \n" + e,
                    "Connection error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    }

    @Override
    public void getUserMessages(String userMessage) {
        try {
            // if the server response to the connection attempt with a HTTP 200 code
            myApiClient.pushMessageToServer(userMessage);

        } catch (Exception e) {
            // error dialog windows in case of failure
            JOptionPane.showMessageDialog(connectionErrorJFrame,
                    "Error while trying to send the message: \n" + e,
                    "Connection error",
                    JOptionPane.ERROR_MESSAGE);

            chatMessageFeed.append("Message not delivered:" + userMessage);
        }
    }

    @Override
    public void displayMessageHistory() {
    }

    /**
     * the user interface pick the userlist queue built by the client from server sent events,it then updates whatever
     * graphical elements it requires in order to display this list based on the difference between the "local" displayed
     * list and the ones sent by the server
     * @param userListQueue a queue generated by the client which contain the list of connected user sent by the server
     *                      on a time interval basis
     */
    @Override
    public void displayConnectedUserList(Queue<List<User>> userListQueue) {
        List<User> freshUserList = userListQueue.poll();
        if (freshUserList != null) {
            freshUserList.forEach(this::refreshConnectedUserList);
        }
    }

    /**
     *
     * @param newMessageQueue a queue constructed by the client, containing lists of messages from the server bufffer
     */
    @Override
    public void displayNewMessages(Queue<List<Message>> newMessageQueue) {
        List<Message> freshMessageList = newMessageQueue.poll();
        if (freshMessageList != null) {
            freshMessageList.forEach(this::refreshSessionMessageList);
        }
    }

    private void refreshConnectedUserList(User usr) {
        // todo : compare users based on name is not smart --> use a session or global ID instead
        if (!(localUserList.contains(usr.userName))) {
            connectedUserList.append(usr.userName + "\n");
            localUserList.add(usr.userName);
        }
    }

    private void refreshSessionMessageList(Message msg) {
        if (!(localMsgList.contains(msg.msgId))) {
            chatMessageFeed.append(msg.userName + " : ");
            chatMessageFeed.append(msg.payload + "\n");
            localMsgList.add(msg.msgId);
        }
    }

    @Override
    public void run() {
        while (true) {
            connectedUserListQueue = myApiClient.getUserEventsQueue();
            sessionMessagesQueue = myApiClient.getNewMessageQueue();

            displayConnectedUserList(connectedUserListQueue);
            displayNewMessages(sessionMessagesQueue);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
