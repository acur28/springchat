package com.webclient.client;


import com.webclient.model.Message;
import com.webclient.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalTime;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Component("myApiClient")
public class ApiClient {
    private static final Logger LOG = LoggerFactory.getLogger(ApiClient.class);
    private final WebClient client;
    private User connectedClientUser;
    private Queue<List<User>> userEventsQueue;
    private Queue<List<Message>> newMessageQueue;

    private String target;

    @Autowired
    public ApiClient(WebClient webClient) {
        client = webClient;
        userEventsQueue = new ConcurrentLinkedQueue<>();
        newMessageQueue = new ConcurrentLinkedQueue<>();
    }

    public boolean connectToServer(String userName, String address) {
        target = "http://" + address + ":8080";
        User connectingUser = new User();
        connectingUser.userName = userName;
        HttpStatus status = establishConnection(connectingUser);
        if (status.equals(HttpStatus.OK)) {
            consumeUserEvent();
            consumeNewMessageEvents();
            connectedClientUser = connectingUser;
            return true;
        } else {
            return false;
        }
    }

    private HttpStatus establishConnection(@RequestBody User connectingUser) {
        ParameterizedTypeReference<String> type
                = new ParameterizedTypeReference<>() {
        };

        String loginEndPoint = target + "/usr";
        Mono<ClientResponse> monoResponse = client.method(HttpMethod.POST)
                .uri(loginEndPoint)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(connectingUser)
                .exchange();

        String response;
        try {
            response = monoResponse
                    .flatMap(clientResponse -> clientResponse.toEntity(type))
                    .block()
                    .getBody();
            LOG.info("Server response : " + response);
        } catch (Exception e) {
            LOG.warn("Unable to connect to the chat server");
            return HttpStatus.NOT_FOUND;
        }
        return HttpStatus.OK;
    }

    public void pushMessageToServer(String payload) {
        Message msgToSend = new Message(connectedClientUser.userName, payload);
        if (connectedClientUser.userName != "") {
            msgToSend.userName = connectedClientUser.userName;
            msgToSend.payload = payload;

            String messageEndPoint = target + "/msg";
            Mono<String> monoResponse = client.method(HttpMethod.POST)
                    .uri(messageEndPoint)
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(msgToSend)
                    .retrieve()
                    .bodyToMono(String.class);

            String response = monoResponse.block();
            LOG.info("Server response : " + response);

        }
    }

    private void consumeUserEvent() {
        ParameterizedTypeReference<ServerSentEvent<List<User>>> type
                = new ParameterizedTypeReference<>() {
        };

        Flux<ServerSentEvent<List<User>>> eventStream = client.get()
                .uri(target + "/stream-userlist")
                .retrieve()
                .bodyToFlux(type);

        eventStream.subscribe(
                content -> {
                    LOG.info("Time: {} , user list: {} ",
                            LocalTime.now(), content.data().toString());
                    userEventsQueue.add(content.data());
                },
                error -> LOG.error("Error receiving user SSE: {}", error),
                () -> LOG.info("Completed!!!"));
    }


    private void consumeNewMessageEvents() {
        ParameterizedTypeReference<ServerSentEvent<List<Message>>> type
                = new ParameterizedTypeReference<>() {
        };

        Flux<ServerSentEvent<List<Message>>> eventStream = client.get()
                .uri(target + "/stream-msg")
                .retrieve()
                .bodyToFlux(type);

        eventStream.subscribe(
                content -> {
                    LOG.info("Time: {} , new message list: {} ",
                            LocalTime.now(), content.data().toString());
                    newMessageQueue.add(content.data());
                },
                error -> LOG.error("Error receiving message SSE: {}", error),
                () -> LOG.info("Completed!!!"));
    }

    public User getConnectedClientUser() {
        return connectedClientUser;
    }

    public Queue<List<Message>> getNewMessageQueue() {
        return newMessageQueue;
    }

    public Queue<List<User>> getUserEventsQueue() {
        return userEventsQueue;
    }
}

