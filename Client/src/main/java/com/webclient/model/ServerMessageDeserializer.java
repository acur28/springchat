package com.webclient.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@JsonComponent
public class ServerMessageDeserializer extends JsonDeserializer<Message> {

    @Override
    public Message deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {

        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        TextNode userName = (TextNode) treeNode.get("userName");
        TextNode payload = (TextNode) treeNode.get("payload");
        TextNode refId = (TextNode) treeNode.get("refId");
        TextNode stamp = (TextNode) treeNode.get("stamp");

        UUID convertedRefID = UUID.fromString(refId.asText());
        Timestamp convertedStamp = Timestamp.valueOf(stamp.asText());

        return new Message(userName.asText(), payload.asText(), convertedStamp, convertedRefID);
    }
}
