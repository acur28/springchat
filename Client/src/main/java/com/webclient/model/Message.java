package com.webclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;
import java.util.UUID;

@JsonDeserialize(using = ServerMessageDeserializer.class)
public class Message {
    public String userName;
    public String payload;
    public UUID msgId;
    public Timestamp stamp;

    public Message(String userName, String payload) {
        this.payload = payload;
        this.userName = userName;
        this.msgId = null;
        this.stamp = null;
    }
    public Message(String userName, String payload, Timestamp stamp, UUID refID) {
        this.payload = payload;
        this.userName = userName;
        this.msgId = refID;
        this.stamp = stamp;
    }

    @Override
    public String toString() {
        return "[" +
                "userName=" + userName +
                ", payload=" + payload +
                "]";
    }
}
