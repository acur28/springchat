package com.springserver.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springserver.model.StampedMessage;
import com.springserver.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;


@RestController
public class ChatController {
    Logger LOG = LoggerFactory.getLogger(ChatController.class);
    ObjectMapper mapper = new ObjectMapper();

    public final IChatService myChatService;

    @Autowired
    public ChatController(IChatService chatService) {
        myChatService = chatService;
    }

    @GetMapping("/msg")
    public ResponseEntity<List<StampedMessage>> getMessageList() {
        return ResponseEntity.ok(myChatService.getBufferedMessageList());
    }

    @GetMapping("/usr")
    public ResponseEntity<List<User>> getUserList() throws JsonProcessingException {
        LOG.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(myChatService.getUserList()));
        return ResponseEntity.ok(myChatService.getUserList());
    }

    @PostMapping("/msg")
    public ResponseEntity<String> addMessage(@RequestBody StampedMessage msg) {  //String userName, @RequestBody String payload) {
        try {
            myChatService.addMessage(msg);
            return ResponseEntity.ok("message delivered");
        } catch (HttpMessageNotReadableException e) {
            return ResponseEntity.badRequest().body("unable to deliver the message because of an error: " + e);
        }
    }

    @PostMapping("/usr")
    public ResponseEntity<String> addUser(@RequestBody User usr) {
        try {
            myChatService.addUser(usr);
            return ResponseEntity.ok("Succesfully connected to the chat");
        } catch (HttpMessageNotReadableException e) {
            return ResponseEntity.badRequest().body("Unable to join because of an error: " + e);
        }
    }

    @PutMapping("/usr")
    public ResponseEntity<String> rmUser(@RequestBody User user) {
        try {
            myChatService.rmUser(user);
            return ResponseEntity.ok("Succesfully logged out");
        } catch (HttpMessageNotReadableException e) {
            return ResponseEntity.badRequest().body("Unable to logout because of an error: " + e);
        }
    }

    @GetMapping("/stream-userlist")
    public Flux<ServerSentEvent<List<User>>> streamUserEvents() {
        return Flux.interval(Duration.ofSeconds(5))
                .map(tmp -> ServerSentEvent.<List<User>>builder()
                        .data(myChatService.getUserList())
                        .build());
    }
    @GetMapping("/stream-msg")
    public Flux<ServerSentEvent<List<StampedMessage>>> streamNewMessageEvents() {
        return Flux.interval(Duration.ofSeconds(5))
                .map(tmp -> ServerSentEvent.<List<StampedMessage>>builder()
                        .data(myChatService.getBufferedMessageList())
                        .build());
    }

}

