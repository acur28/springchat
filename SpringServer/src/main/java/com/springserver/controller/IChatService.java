package com.springserver.controller;

import com.springserver.model.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IChatService {
 List<StampedMessage> getHistoryMessageList();
 List<StampedMessage> getBufferedMessageList();
 List<User> getUserList();
 void flushMessageBuffer();
 // void addMessage(String userName, String msgPayload);
 void addMessage(StampedMessage message);
 void addUser(User usr);
 void rmUser(User usr);

}
