package com.springserver.controller;

import com.springserver.model.User;

public interface IUserLoginManager {
    boolean checkUserAccessAuthorization(User user, String password);
    void logConnectionEvent();

}
