package com.springserver.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@JsonDeserialize(using = UserDeserializer.class)
public class User {
    public String userName;
    private final UUID userID;

    public User(String userName) {
        this.userName = userName;
        userID = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return "userID= " + userID + ",login: " + userName;
    }

    /**
     * Check the whitelist for service access authorization
     *
     * @return the authorization status (true = user authorized)
     */
    public boolean verifyAccess() {
        return true;
    }
}
