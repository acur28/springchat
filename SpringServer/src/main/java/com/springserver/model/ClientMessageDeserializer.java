package com.springserver.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@JsonComponent
public class ClientMessageDeserializer extends JsonDeserializer<StampedMessage> {

    @Override
    public StampedMessage deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {

        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        TextNode userName = (TextNode) treeNode.get("userName");
        TextNode payload = (TextNode) treeNode.get("payload");

        Timestamp ts = generateMessageTs();
        UUID refId = generateMessageRefId();

        return new StampedMessage(userName.asText(), payload.asText(), ts, refId);
    }

    public Timestamp generateMessageTs() {
        Date date = new Date();
        return new Timestamp(date.getTime());
    }

    public UUID generateMessageRefId() {
        return UUID.randomUUID();
    }

}
