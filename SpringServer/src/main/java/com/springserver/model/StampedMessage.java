package com.springserver.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.sql.Timestamp;
import java.util.UUID;

@JsonDeserialize(using = ClientMessageDeserializer.class)
@JsonSerialize(using = ClientMessageSerializer.class)
public class StampedMessage {

    public UUID refId;
    public Timestamp msgTimeStamp;
    public String userName;
    public String payload;

    public StampedMessage(String userName, String payload, Timestamp msgTimeStamp, UUID refId) {
        this.userName = userName;
        this.payload = payload;
        this.msgTimeStamp = msgTimeStamp;
        this.refId = refId;
    }


}
