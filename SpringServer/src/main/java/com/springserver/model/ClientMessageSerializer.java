package com.springserver.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ClientMessageSerializer extends JsonSerializer<StampedMessage> {
    @Override
    public void serialize(StampedMessage stampedMessage, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("refId", stampedMessage.refId.toString());
        jsonGenerator.writeStringField("userName", stampedMessage.userName);
        jsonGenerator.writeStringField("payload", stampedMessage.payload);
        jsonGenerator.writeStringField("stamp", stampedMessage.msgTimeStamp.toString());
        jsonGenerator.writeEndObject();
    }
}