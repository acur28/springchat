package com.springserver.service;

import com.springserver.controller.IChatService;
import com.springserver.model.StampedMessage;
import com.springserver.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ChatService implements IChatService {
    private List<User> users;
    private List<StampedMessage> bufferedStampedMessages;
    Logger LOG = LoggerFactory.getLogger(ChatService.class);


    public ChatService() {//RestController restController){
        System.out.println("***Starting Chat Service***");
        users = new ArrayList<>();
        bufferedStampedMessages = new ArrayList<>();
    }

    @Override
    public List<StampedMessage> getHistoryMessageList() {
        return null;
    }

    @Override
    public List<StampedMessage> getBufferedMessageList() {
        return bufferedStampedMessages;
    }

    /**
    *Returns the currently connected user list
     * @return the list of connected users
    */
    @Override
    public List<User> getUserList() {
        return users;
    }

    /**
    Transfer the buffered messages list to the database and clear the message buffer
    */
    @Override
    public void flushMessageBuffer() {
        // transfer the list of message to the database

        // clear the list
        bufferedStampedMessages.clear();
    }

    /**
     * add a new message to the message buffer
     * @param msg the message, received from the client, to be added to the buffer
     */
    @Override
    public void addMessage(StampedMessage msg) {
        bufferedStampedMessages.add(msg);
    }

    /**
     * add a new user to the list of connected users
     * @param usr the user object received from the client
     */
    @Override
    public void addUser(User usr) {
        users.add(usr);
        LOG.info("User " + usr.userName + " joined the chat");
    }

    /**
     * remove a connected user from the list
     * @param usr a user object
     */
    @Override
    public void rmUser(User usr) {
        users.remove(usr);
        LOG.info("User " + usr.userName + " left the chat");
    }


}
